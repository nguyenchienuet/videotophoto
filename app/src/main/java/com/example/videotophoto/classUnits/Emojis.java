package com.example.videotophoto.classUnits;

public class Emojis {
    String name;

    public Emojis(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
