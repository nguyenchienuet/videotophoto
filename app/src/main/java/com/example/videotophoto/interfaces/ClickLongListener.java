package com.example.videotophoto.interfaces;

import android.media.Image;

import com.example.videotophoto.classUnits.Images;

public interface ClickLongListener {
    void onLongClickImage(Images images);
}
